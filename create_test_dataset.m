function create_test_dataset(study_dir, src_dicom_dir)

    subject_dir = fullfile(study_dir, 'subject001', 'visit1');
    dst_dicom_dir = fullfile(subject_dir, 'DICOM');
    mkdir(dst_dicom_dir);
    
    dicom_dict = fullfile(study_dir, 'dicom-dict.txt');
    save_imgs(src_dicom_dir, dst_dicom_dir, dicom_dict);
end
%%
function img_list = get_img_list(src_dicom_dir)
    img_list = cell(0,1);
    subdirs = genpath(src_dicom_dir);
    splits = find(subdirs == ';');
    dir_start = 1;
    for i_dir = 1:length(splits)
        dirname = subdirs(dir_start:splits(i_dir)-1);
        img_files = dir(fullfile(dirname, 'IM*'));
        if ~isempty(img_files)
            img_names = {img_files(:).name}';
            img_paths = strcat(dirname, filesep, img_names);
            img_list = [img_list; img_paths]; %#ok
        end
        dir_start = splits(i_dir)+1;
    end
end
%%
function save_imgs(src_dicom_dir, dst_dicom_dir, dicom_dict)
    img_list = get_img_list(src_dicom_dir);
    n_imgs = length(img_list);
    for i_img = 1:n_imgs
        fprintf('Saving image %d of %d\n', i_img, n_imgs);
        img_name = img_list{i_img};
        img = dicomread(img_name);
        info = dicominfo(img_name);
        info = strip_private(info);
        
        info.PatientBirthDate = '20000101';
        info.PatientID = '0001';
        info.PatientName.FamilyName = 'Madym';
        info.PatientWeight = 70;

        dst_name = sprintf('%s%sIM%05d', dst_dicom_dir, filesep, i_img);
        dicomdict('set', dicom_dict);
        dicomwrite(img, dst_name, info, 'WritePrivate', true);
    end
end
%%
function info = strip_private(info)
    fields = fieldnames(info);
    for i_f = 1:length(fields)
        f = fields{i_f};
        if contains(f, 'Private') && ...
                ~strcmpi(f, 'Private_2005_100d') && ...
                ~strcmpi(f, 'Private_2005_100e')
            info = rmfield(info, f);
        end
    end
end
            

