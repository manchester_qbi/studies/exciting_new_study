@ECHO OFF

::Run madym DICOM convert tool
%MADYM_ROOT%\madym_DicomConvert --config %MADYM_CONFIG_DIR%\madym_dicom_make_config.txt %*

::Run python viewer
python -m QbiPy.tools.simple_3D_viewer.simple_3D_viewer_tool dynamic 0