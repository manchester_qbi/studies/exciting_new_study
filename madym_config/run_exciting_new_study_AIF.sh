#!/bin/bash

#Run madym AIF
$MADYM_ROOT/madym_AIF --config $MADYM_CONFIG_DIR/madym_AIF_config.txt "$@"

#Run python viewer
python -m QbiPy.tools.AIF_viewer.AIF_viewer_tool madym_output/AIF dynamic/dyn_10.hdr
