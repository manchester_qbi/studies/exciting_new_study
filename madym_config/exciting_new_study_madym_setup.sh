#!/bin/bash

#Name this study
export STUDY_NAME=exciting_new_study

#Uncomment to set the specific version of Madym used in this study
#if you have multiple versions of Madym installed
#export MADYM_ROOT="/usr/madym-v4.17.1/bin"

#Set variable to study root
export MADYM_CONFIG_DIR="$PWD/`dirname \"$BASH_SOURCE\"`"

#Add this folder to the path
export PATH=$PATH:$MADYM_CONFIG_DIR

echo '******************************************************'
echo Set Madym environment for study: $STUDY_NAME
echo Using madym version:
$MADYM_ROOT/madym_DCE -v
echo '******************************************************'
