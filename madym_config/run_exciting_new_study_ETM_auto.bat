@ECHO OFF
::Get tumour and slice from args
SET TUMOUR=%1
SET AIF=madym_output\AIF\%2_Auto_AIF.txt

::Remove the first 2 args and store the rest
shift
shift
set EXTRA_PARAMS=%1
:loop
shift
if [%1]==[] goto afterloop
set EXTRA_PARAMS=%EXTRA_PARAMS% %1
goto loop
:afterloop

::Run madym DCE
%MADYM_ROOT%\madym_DCE --config %MADYM_CONFIG_DIR%\madym_ETM_auto_config.txt --roi roi\%TUMOUR% -o %TUMOUR% --aif %AIF% %EXTRA_PARAMS%

::Run python DCE fit viewer
python -m QbiPy.tools.DCE_fit_viewer.DCE_fit_viewer_tool madym_output\ETM_auto\%TUMOUR%
python -m QbiPy.tools.simple_3D_viewer.simple_3D_viewer_tool madym_output\ETM_auto\%TUMOUR% 2