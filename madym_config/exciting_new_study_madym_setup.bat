@ECHO OFF

::Name this study
SET STUDY_NAME=exciting_new_study

::Uncomment this to set the specific version of Madym used in this study
::if you have multiple versions of Madym installed
::SET MADYM_ROOT="C:\Program Files\Madym\madym-v4.17.1\bin"

::Set variable to study root
SET MADYM_CONFIG_DIR=%~dp0

::Set this folder to the path
SET PATH=%PATH%;%~dp0

ECHO ******************************************************
ECHO Set Madym environment for study: %STUDY_NAME%
ECHO Using madym version:
%MADYM_ROOT%\madym_DCE -v
ECHO ******************************************************