@ECHO OFF

::Run madym T1
%MADYM_ROOT%\madym_T1 --config %MADYM_CONFIG_DIR%\madym_T1_config.txt %*

::Run python viewer
python -m QbiPy.tools.simple_3D_viewer.simple_3D_viewer_tool madym_output/T1 2 .nii.gz