# exciting_new_study

Test dataset to show how Madym can be used in formal research studies. Contains an anonymised DICOM dataset, and a set of config files and bash/shell scripts for processing the data.

## Getting started
To run this demonstration:

1. Make sure you have Madym [installed](https://gitlab.com/manchester_qbi/manchester_qbi_public/madym_cxx/-/wikis/prebuilt_binaries) or [built from source](https://gitlab.com/manchester_qbi/manchester_qbi_public/madym_cxx/-/wikis/build_instructions) and the [python tools installed](https://gitlab.com/manchester_qbi/manchester_qbi_public/madym_cxx/-/wikis/python_getting_started)
2. Download/clone this repository
3. Go to the folder `subject001/visit1` and unzip `DICOM.zip` to generate the individual DICOM images
4. Open a command window and `cd` into this repository. For all further steps use `.bat` scripts for Windows and `.sh` scripts on other operating systems
5. Run `madym_config/exciting_study_setup`. This should finish with a message (the exact Madym version may differ):

        ******************************************************
        Set Madym environment for study: exciting_new_study
        Using madym version:
        v4.16.1
        ******************************************************

6. `cd` into `subject001/visit1`. To process this data, try running:
    - `run_exciting_new_study_dicom_sort`
    - `run_exciting_new_study_dicom_make`
    - `run_exciting_new_study_T1`
    - `run_exciting_new_study_ETM_pop tumour1`

See [here](https://gitlab.com/manchester_qbi/manchester_qbi_public/madym_cxx/-/wikis/study_analysis) for a detailed description of this example and how it can be used in formal studies.


